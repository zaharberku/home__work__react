import React from "react";
import Basket from "./components"
import { GoBack } from "../../sharedComponents";

const BasketPage = (props) =>{
   const {data, totalPrice, openModal} = props
    return(
    <section>
        <GoBack/>
     <Basket data={data} totalPrice={totalPrice} openModal={openModal}/>
    </section>
)}


export default BasketPage