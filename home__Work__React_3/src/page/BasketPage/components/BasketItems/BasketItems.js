import React from "react";
import style from "./BasketItems.module.scss";
import {Img, Button} from "../../../../sharedComponents";
import PropTypes from "prop-types";
import {dataBtnRemove} from "../../../../data";

const BasketItems = (props) =>{
    const {data, openModal} = props
    const { name, src, price,count } = data
    return(
    <div className={style.prodBasket}>
        <Img additionalClassNames={style.basketImg} src={src} alt={name}/>
        <p className={style.name}>{name}</p>
        <p className={style.price}>{price}</p>
        <p>Количество:{count}</p>
        <Button additionalClassNames={style.btnRemove} data={dataBtnRemove} handelClick={()=>openModal(dataBtnRemove.id, data)}/>
    </div>
)};


BasketItems.defaultProps = {
    name: 'Ноут',
    src: 'Картинка',
    price: '15000',
    count: 0,
};

BasketItems.propTypes = {
    name: PropTypes.string,
    src: PropTypes.string,
    price: PropTypes.string,
    count: PropTypes.number,
};




export default BasketItems;