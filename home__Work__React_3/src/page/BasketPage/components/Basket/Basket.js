import React from "react";
import BasketItems from "../BasketItems";
import style from "./Basket.module.scss";
import PropTypes from "prop-types";


const Basket = ({ data, totalPrice, openModal }) => {
    return (
        <div className={style.containerBasket}>
            <p>Total price:{totalPrice}</p>
            {data.map(element => {
                return <BasketItems key={element.id} openModal={openModal} data={element} />
            })}
        </div>
    )
};


Basket.defaultProps = {
    data: [],
    totalPrice: 0,
};


Basket.propTypes = {
    data: PropTypes.arrayOf(PropTypes.object),
    totalPrice: PropTypes.number,
};


export default Basket;