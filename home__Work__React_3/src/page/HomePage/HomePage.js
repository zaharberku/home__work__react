import React from "react";
import Card from "./components";


const Home = (props) =>{
    const {dataForProducts, openModal, basket, totalPrice, changesDataLocalStor} = props
    return(
    <section>
        <Card changesDataLocalStor={changesDataLocalStor} dataForProducts={dataForProducts} openModal={openModal} basket={basket} totalPrice={totalPrice}/>
    </section>
)};


export default Home;