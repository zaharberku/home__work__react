import React from "react";
import CardItems from "../CardItems";
import style from "./Card.module.scss";
import PropTypes from "prop-types";


const Card = (props) =>{
const {dataForProducts, openModal, changesDataLocalStor} = props
return (
  <div className={style.containerProd}>
    <div className={style.containerCard}>
      { dataForProducts.map(element =>
      <CardItems 
        key={element.id}
        data={element} 
        openModal={openModal}
        changesDataLocalStor={changesDataLocalStor}
        dataProd={dataForProducts}/>
        )}
    </div>
  </div>
)};

Card.defaultProps = {
  dataForProducts: [],
  openModal: ()=>{},
  favorite:[],
};

Card.propTypes = {
  dataForProducts: PropTypes.arrayOf(PropTypes.object),
  openModal: PropTypes.func,
  favorite:PropTypes.arrayOf(PropTypes.object),
};


export default Card;