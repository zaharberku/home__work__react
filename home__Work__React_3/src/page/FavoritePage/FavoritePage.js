import React from "react";
import Favorite from "./components";
import { GoBack } from "../../sharedComponents";


const FavoritePage = ({data}) =>{
    const filterFavoritProd = () =>{
        return data.filter(element=> element.favorite)
    };
    return(
    <section>
        <GoBack/>
        <Favorite data={filterFavoritProd()}/>
    </section>
)};


export default FavoritePage;