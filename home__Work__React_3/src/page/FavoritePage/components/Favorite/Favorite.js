import React from "react";
import FavoriteItems from "../FavoriteItems";


const Favorite = ({data}) =>{
    return(
        <div>
            {data.map(element=> <FavoriteItems key={element.id} data={element}/>)}
        </div>
    );
};


export default Favorite;