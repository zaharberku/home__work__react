import HomePage from "./HomePage";
import FavoritePage from "./FavoritePage";
import BasketPage from "./BasketPage";

export {HomePage, FavoritePage, BasketPage};