import { Modal, Main, Header, HeaderNav } from './sharedComponents';
import React, { useEffect, useState } from "react";
import { modalWindowDeclarations } from "./data";
import { getLocalStorage, setLocalStorage } from "./util";
import Router from './router';





const App = () => {
  const [isOpenModal, setOpenModal] = useState(false);
  const [dataForModal, setDataForModal] = useState({});
  const [data, setData] = useState([]);
  const [product, setProduct] = useState({});
  const [basket, setBasket] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);

  useEffect(async () => {
    let data = getLocalStorage("data");
    const dataBasket = getLocalStorage('basket') ;
    if (data.length === 0) {
      try{
        data = await fetch('./data.json').then(res => res.json());
        data.forEach(element => setLocalStorage("data", element));
      }catch(error){
        console.log(error.name);
      }
    };
    setData(data);
    setBasket(dataBasket);
    setTotalPrice(dataBasket.reduce((item, element) => item + (+element.price.replace(' ', '') * element.count), 0));
  }, []);

  const closeModal = () => {
    setOpenModal(false)
    setDataForModal({})
  };

  const openModal = (id, data) => {
      setOpenModal(true);
      setDataForModal(modalWindowDeclarations.find(element => element.id === +id));
      setProduct(data);
  };

  const changesDataLocalStor = ({id:idElem}, bool) => {
    setData(data=>{
      const newData = [...data]
      const index = data.findIndex(({id}) => id === idElem)
      newData[index].favorite = bool
      localStorage.setItem('data', JSON.stringify(data))
      return newData
    })
  }

  const addProductBasket = ({ name, id, pathPicture: src, price}) =>{
    setBasket(basket =>{
      const dataProd = {
        name,
        id,
        src,
        count: 1,
        price,
      };

      const newBasket = [...basket];
      const index = newBasket.findIndex(element => element.id === id);

      if (index === -1) {
        newBasket.push(dataProd);
        setTotalPrice(totalPrice => totalPrice + +price.replace(" ", ''));
        setLocalStorage('basket', dataProd);
        closeModal();
        return newBasket;
      };
      newBasket[index].count = newBasket[index].count += 1;
      setTotalPrice(totalPrice => totalPrice + +price.replace(" ", ''));
      localStorage.setItem('basket', JSON.stringify(newBasket));
      closeModal();
      return newBasket;
    });
  };

  const removeProductBasket = ({id, count, price}) =>{
    setBasket(basket =>{
      const newBasket = [...basket]
      const index = newBasket.findIndex(element => element.id === id)
      newBasket.splice(index, 1)
      localStorage.setItem('basket', JSON.stringify(newBasket))
      closeModal()
      setTotalPrice(totalPrice => totalPrice - +price.replace(" ", '') * count)
      return newBasket
    });
  };

  const lisenerBtnModal = (keys, data) =>{
    switch(keys){
        case 1:
          return  addProductBasket(data);
        case 2:
          return  removeProductBasket(data);
        default:
          return null  
    }
  }

  return (
    <div className="container">
      <Header>
        <HeaderNav/>  
      </Header>
      <Main>
        <Router setData={setData} dataForProducts={data} openModal={openModal} basket={basket} totalPrice={totalPrice} changesDataLocalStor={changesDataLocalStor}/>
        {isOpenModal && <Modal closeModal={closeModal} dataForModal={dataForModal}  product={product} lisenerBtnModal={lisenerBtnModal}/>}
      </Main>
    </div>
  );

}



export default App;
