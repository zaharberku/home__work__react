import React from "react";
import PropTypes from "prop-types";
import style from "./Modal.module.scss";
import Button from "../Button";
import { dataCloseBtn } from "../../data";
import { setLocalStorage } from "../../util";

const Modal = (props) =>{
    const { dataForModal: {actions, text, title, id }, closeModal, product, lisenerBtnModal } = props;
    const closeBtn = dataCloseBtn.find( elem=> elem.id === id);
    return(
        <div onClick={closeModal} className={style.windowClose}>
        <div className={style.modal}>
            <div className={style.modalHeader}>
                <h1 className={style.modalTitle}>{title}</h1>
            </div>
            <div className={style.modalBody}>
                {`${text} ${product.name}`}
            </div>
            <div className={style.modalFooter}>
                {actions(()=>{lisenerBtnModal(id, product)})}
                {id === 1 && <Button additionalClassNames={style.closeBtn} handelClick={closeModal} data={closeBtn}/>}
            </div>
        </div>
        </div>
    );
};

Modal.defaultProps = {
    dataForModal: {
        actions: <button></button>,
        text: 'text',
        title: 'title',
        id:0,
    },
    closeModal:()=>{},
    product:{},
    addProductBasket:()=>{},
};

Modal.protoType = {
    dataForModal: PropTypes.shape({
        actions: PropTypes.element,
        text: PropTypes.string,
        title: PropTypes.string,
        id: PropTypes.number,
    }),
    closeModal: PropTypes.func,
    product: PropTypes.object,
    addProductBasket: PropTypes.func,
};


export default Modal;