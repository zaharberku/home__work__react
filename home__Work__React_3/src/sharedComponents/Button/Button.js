import React from "react";
import PropTypes from "prop-types";
import style from "./Button.module.scss";
import classNames from "classnames"

const Button = (props) => {
    const {data:{ text, background }, handelClick, additionalClassNames} = props;
    return(
        <button className={classNames(style.btn, additionalClassNames)} onClick={handelClick} background={background}>{text}</button>

    );
};

Button.defaultProps = {
    data: {
        background: '#fff',
    },
    handelClick: ()=>{},
    className: 'btn',
};

Button.propTypes = {
    data: PropTypes.shape({
        text: PropTypes.string.isRequired,
        background: PropTypes.string,
    }),
    handelClick: PropTypes.func,
    className: PropTypes.string,
};

export default Button;

