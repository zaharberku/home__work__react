import React from "react";
import style from "./Main.module.scss";
import PropTypes from "prop-types";

const Main = ({children}) =>(
    <main className={style.main}>
        {children}
    </main>
);

Main.defaultProps = {
    children:null
};

Main.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
};

export default Main;