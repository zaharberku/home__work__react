import React from "react";
import styles from "./List.module.scss";
import {NavLink} from "react-router-dom";

const List = ({text, path}) =>(
    <NavLink exact activeClassName={styles.active} to={path}>{text}</NavLink>
);

export default List;