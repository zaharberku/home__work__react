import style from "../sharedComponents/Modal/Modal.module.scss";


const dataHeaderNav = [
  {
    id:1,
    text:'Home',
    path:'/'
  },
  {
    id:2,
    text:'Basket',
    path:'/basket'
  },
  {
    id:3,
    text:'Favorite',
    path:'/favorite'
  },
];

const modalWindowDeclarations = [
  {
    id: 1,
    text: 'Add basket',
    title: 'Basket',
    actions: (func)=><button onClick={func} className={style.modalFooterBtn}>Save</button>,
  },
  {
    id: 2,
    text: 'Remove basket',
    title: 'Remove',
    actions: (func) => <button onClick={func} className={style.btnRemove}>Remove</button>,
  },
];


const dataCloseBtn = [
  {
    text: "Cancel",
    background: "black",
    id: 1,
  },
  {
    text: "Cancel",
    background: "black",
    id: 2,
  }

];

const dataBtnCard = {
  id: 1,
  background: "#e8b692",
  text: "Add to cart",
}
const dataBtnRemove = {
  id: 2,
  background: "red",
  text: "Remove",
};

export { modalWindowDeclarations, dataCloseBtn, dataBtnCard, dataHeaderNav, dataBtnRemove };