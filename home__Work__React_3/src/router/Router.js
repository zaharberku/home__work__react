import React from 'react'
import {Route, Switch} from "react-router-dom"
import { HomePage, FavoritePage, BasketPage } from '../page'

const Router = (props) =>{
    const {dataForProducts, openModal, basket, totalPrice, changesDataLocalStor} = props
    return(
    <Switch>
        <Route exact path="/" render={(props)=><HomePage {...props} changesDataLocalStor={changesDataLocalStor} dataForProducts={dataForProducts} openModal={openModal}/>}/>
        <Route exact path="/basket" render={(props)=><BasketPage {...props} data={basket} totalPrice={totalPrice} openModal={openModal}/>}/>
        <Route exact path="/favorite" render={(props)=><FavoritePage {...props} data={[...dataForProducts]}/>}/>
    </Switch>
)}

export default Router