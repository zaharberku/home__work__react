import './App.scss';
import {Button, Modal} from './components'
import React,{Component} from "react";



class App extends Component{
  state = {
    flag:false,
    text: null,
    dataForBtn:{
      background:"#e8b692",
      textBtnFirst:"Open first modal",
      textBtnSecond:"Open second modal",
    },
    dataForModal:null
  }

  modalWindow=()=>{
    const modalWindowDeclarations = [
      {
        id:1,
        text:'Modal First',
        title:'Title',
        actions:<button className="modal__footer-btn">Save</button>,
        closeButton:<button onClick={this.closeModal.bind(this)} className="modal__header-btn">X</button>,
      },
      {
        id:2,
        text:'Second First',
        title:'Title2',
        actions:<button className="modal__footer-btn footer-btn">More</button>,
        closeButton:<button onClick={this.closeModal.bind(this)} className="modal__header-btn header-btn">X</button>
      },
    ]
    return modalWindowDeclarations
  }
  closeModal(){
    this.setState({
      flag:false,
      dataForModal:null
    })
  }
  openModal = (event) =>{
    const {id} = event.target.dataset
    this.setState({
      flag:true,
      dataForModal:this.modalWindow().find(element => element.id === +id)
    })
  }
  render(){
    const {dataForBtn, dataForModal, flag, text} = this.state
    return (
      <div className="App">
        <Button dataForBtn={dataForBtn} openModal={this.openModal}/>
        <main className='App__main main'>
          {flag && <div onClick={this.closeModal.bind(this)} className='main__window-close'><Modal dataForModal={dataForModal} text={text}/></div>}
        </main>
      </div>
    );
  }
 
}

export default App;
