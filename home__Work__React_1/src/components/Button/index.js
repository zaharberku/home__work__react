import React,{Component} from "react";


class Button extends Component{
    render(){
        const{dataForBtn:{textBtnFirst,background,textBtnSecond},openModal}= this.props;

        return(
                <header className="App__header header">
                <button className="header__btn" data-id="1" onClick={(event)=>openModal(event)} background={background}>{textBtnFirst}</button>
                <button className="header__btn" data-id="2" onClick={(event)=>openModal(event)} background={background}>{textBtnSecond}</button>
                </header>
        )
    }
}

export default Button