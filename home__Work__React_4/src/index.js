import React from 'react';
import ReactDOM from 'react-dom';
import "./style/index.scss";
import App from './App';
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from 'react-redux';
import rootStore from "./store/reducers"


ReactDOM.render(
  <Provider store={rootStore}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById('root')
);

