import { getLocalStorage, setLocalStorage } from "./localStorage";
import { findIndexElement } from "./findIndex";

export {getLocalStorage, setLocalStorage, findIndexElement}