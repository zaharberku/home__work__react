const getLocalStorage = (keys) =>{
    return JSON.parse(localStorage.getItem(keys) || '[]')
}


export default getLocalStorage