export const findIndexElement = (arr, id) =>{
    return arr.findIndex(element => element.id === id)
}