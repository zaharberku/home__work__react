import { Modal, Main, Header, HeaderNav } from './sharedComponents';
import React, { useEffect } from "react";
import { getLocalStorage } from "./util";
import Router from './router';
import { useSelector, useDispatch } from 'react-redux';
import { getData, getDataLocalStorage, getTotalPrice, getDataLocalStorageBasket } from './store/createActions';





const App = () => {
  const {modal} = useSelector(state => state)
  const dispatch = useDispatch()

  useEffect(() => {
    const data = getLocalStorage("data");
    dispatch(getDataLocalStorageBasket('basket'));
    dispatch(getDataLocalStorage('data'));
    dispatch(getTotalPrice());

    if (data.length === 0) {
      try{
        dispatch(getData())
      }catch(error){
        console.log(error.name);
      }
    };
  }, []);

  return (
    <div className="container">
      <Header>
        <HeaderNav/>  
      </Header>
      <Main>
        <Router />
        { modal.isOpen && <Modal />}
      </Main>
    </div>
  );

}



export default App;
