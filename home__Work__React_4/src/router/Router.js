import React from 'react'
import {Route, Switch} from "react-router-dom"
import { HomePage, FavoritePage, BasketPage } from '../page'

const Router = () =>{
    return(
    <Switch>
        <Route exact path="/" render={(props)=><HomePage {...props}/>}/>
        <Route exact path="/basket" render={(props)=><BasketPage {...props}/>}/>
        <Route exact path="/favorite" render={(props)=><FavoritePage {...props} />}/>
    </Switch>
)}

export default Router