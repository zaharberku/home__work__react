export const REMOVE_PRODUCT_BASKET = 'REMOVE_PRODUCT_BASKET';
export const ADD_PRODUCT_BASKET = 'ADD_PRODUCT_BASKET';
export const GET_DATA_LOCAL_STORAGE_BASKET = 'GET_DATA_LOCAL_STORAGE_BASKET';
export const CALCULATION_TOTAL_PRICE = 'CALCULATION_TOTAL_PRICE';