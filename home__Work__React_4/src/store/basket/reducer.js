import { REMOVE_PRODUCT_BASKET, ADD_PRODUCT_BASKET, GET_DATA_LOCAL_STORAGE_BASKET, CALCULATION_TOTAL_PRICE } from "./actions";
import { setLocalStorage, findIndexElement } from "../../util"

const initialState = {
    basketData: [],
    totalPrice: 0
}


const reducerBasketPage = (state = initialState, actions) => {
    const { type, payload } = actions
    const newBasket = [...state.basketData]
    const index = findIndexElement(newBasket, payload?.id)
    
    switch (type) {

        case GET_DATA_LOCAL_STORAGE_BASKET:
            
            return { ...state, basketData: payload }
        
        case CALCULATION_TOTAL_PRICE:
            
            return { ...state, totalPrice: state.basketData.reduce((item, element) => item + (+element.price.replace(' ', '') * element.count), 0) }
        
        case REMOVE_PRODUCT_BASKET:

            state.totalPrice = state.totalPrice - (+payload.price.replace(' ', '') * payload.count)
            newBasket.splice(index,1)
            localStorage.setItem('basket', JSON.stringify(newBasket))
            return { ...state, basketData: newBasket }

        case ADD_PRODUCT_BASKET:

                state.totalPrice = state.totalPrice + +payload.price.replace(' ', '')
            if (index !== -1) {
                newBasket[index].count = (newBasket[index]?.count || 0) + 1
                localStorage.setItem('basket', JSON.stringify(newBasket))
                return { ...state, basketData: newBasket }
            } else {
                payload.count = 1
                setLocalStorage('basket', payload)
                return { ...state, basketData: [...state.basketData, payload]}
            }

        default:
            return state
    };
};


export default reducerBasketPage