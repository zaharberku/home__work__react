import { CALCULATION_TOTAL_PRICE, GET_DATA_LOCAL_STORAGE_BASKET } from "./actions";
import { getLocalStorage } from "../../util";


export const getDataLocalStorageBasket = (key) => {
    let data = getLocalStorage(key);
    return({type:GET_DATA_LOCAL_STORAGE_BASKET, payload: data});
};

export const getTotalPrice = () =>({type:CALCULATION_TOTAL_PRICE});


export const changesLocalStorDataBasket = (type,payload) => ({type,payload})