import { createStore, combineReducers, applyMiddleware } from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import reducerBasketPage from "./basket/reducer";
import reducerApp from "./appReducer/reducer"
import reducerModal from "./modal/reducer";
import thunk from "redux-thunk"

const reducers = combineReducers({
    basket: reducerBasketPage,
    app: reducerApp,
    modal: reducerModal,
})

const rootStore = createStore( reducers, composeWithDevTools(applyMiddleware(thunk)))


export default rootStore