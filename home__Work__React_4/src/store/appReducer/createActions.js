import { GET_DATA, SET_IS_FAVORITE } from "./actions";
import { setLocalStorage, getLocalStorage } from "../../util";

export const getData = () => async (dispatch) => {
    const data = await fetch('./data.json').then(res => res.json());
    data.forEach(element => setLocalStorage("data", element));
    dispatch({type:GET_DATA, payload: data});
};

export const getDataLocalStorage = (key) => {
    let data = getLocalStorage(key);
    return({type:GET_DATA, payload: data});
};

export const isFavorite = (id) =>({type:SET_IS_FAVORITE, payload:id});