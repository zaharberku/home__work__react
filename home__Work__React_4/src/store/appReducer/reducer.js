import { GET_DATA, SET_IS_FAVORITE } from "./actions";
import { findIndexElement } from "../../util";


const initialState = {
    data: [],
};


const reducerApp = (state = initialState, actions) => {
    const { type, payload } = actions
    const newData = [...state.data]
    const index = findIndexElement(newData, payload)
    switch (type) {
        case GET_DATA:
            return { ...state, data: payload }
        case SET_IS_FAVORITE:
            newData[index].favorite = !newData[index].favorite
            localStorage.setItem('data', JSON.stringify(newData))
            return {...state, data:newData}
        default:
            return state
    };
};


export default reducerApp