//APP
import { getData, getDataLocalStorage, isFavorite } from "./appReducer/createActions";


//MODAL
import { setIsOpenModal, setConfigModal } from "./modal/createActions";



//BASKET
import { getDataLocalStorageBasket, getTotalPrice, changesLocalStorDataBasket } from "./basket/createActions";











export { 
    //APP
    getDataLocalStorageBasket, 
    getTotalPrice,
    isFavorite,
    //MODAL
    setIsOpenModal, 
    setConfigModal,
    //BASKET
    getData, 
    getDataLocalStorage,
    changesLocalStorDataBasket,
};




