import { SET_IS_OPEN_MODAL, SET_CONFIG_OPEN_MODAL } from "./actions";

export const setIsOpenModal = (bool) => ({
    type:SET_IS_OPEN_MODAL,
    payload: bool,
})

export const setConfigModal = (data) => ({
    type:SET_CONFIG_OPEN_MODAL,
    payload: data,
})