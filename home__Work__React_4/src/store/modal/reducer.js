import { SET_IS_OPEN_MODAL, SET_CONFIG_OPEN_MODAL } from "./actions";

const initialState = {
    isOpen: false,
    title:'',
    text:'',
    actions:null,
    id:0,
    key:null,
    prod:{}
};


const reducerModal = (state = initialState, actions) => {
    const { type, payload } = actions
    switch (type) {
        case SET_IS_OPEN_MODAL:
            return {...state, isOpen:payload}
        case SET_CONFIG_OPEN_MODAL:
            return {...state, 
                title: payload.title, 
                id:payload.id, 
                text: payload.text, 
                actions: payload.actions, 
                key: payload.key,
                prod: payload.prod
              }
        default:
            return state
    };
};


export default reducerModal;