import React from "react";
import {dataBtnCard, modalWindowDeclarations} from "../../../../data";
import { Button, Img } from "../../../../sharedComponents";
import {GlobalSvg} from "../../../../assets";
import style from "./CardItems.module.scss";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { setConfigModal, setIsOpenModal, isFavorite } from "../../../../store/createActions";

const CardItems = (props) =>{
  
  const {data} = props
  const {name, pathPicture, price, vendorCode, color, favorite, id} = data
  const dispatch = useDispatch();

  
  return(
    <div className={style.blockProduct}>
    <span className={style.favorite} onClick={()=> dispatch(isFavorite(id))} >
    {favorite ? <GlobalSvg id={"fav"}/> : <GlobalSvg id={"unfav"}/>}
    </span>
    <Img src={pathPicture} alt={name}/>
    <p><span className={style.list}>Название:</span>{name}</p>
    <p><span className={style.list}>Цена:</span>{price}</p>
    <p><span className={style.list}>Код товара:</span>{vendorCode}</p>
    <p><span className={style.list}>Цвет:</span>{color}</p>
    <Button 
    data={dataBtnCard} 
    handelClick={()=>{
      dispatch(setConfigModal({...modalWindowDeclarations[0], prod:data}))
      dispatch(setIsOpenModal(true))
    }} />
    </div>
  );
};

CardItems.defaultProps = {
  data:{
    name: 'name',
    pathPicture: 'pathPicture',
    price: '1000',
    vendorCode: '12313123wqeq',
    color: 'white',
    id:0,
  },
  data:{},
  openModal: ()=>{},
  dataProd: [],
  changesProduct: ()=>{},
  favorite:[],
};

CardItems.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    pathPicture: PropTypes.string,
    price: PropTypes.string,
    vendorCode: PropTypes.string,
    color: PropTypes.string,
    id:PropTypes.number,
  }),
  data: PropTypes.object,
  openModal: PropTypes.func,
  dataProd: PropTypes.arrayOf(PropTypes.object),
  changesProduct: PropTypes.func,
  favorite: PropTypes.arrayOf(PropTypes.object),
};

export default CardItems;