import React from "react";
import CardItems from "../CardItems";
import style from "./Card.module.scss";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";

const Card = () =>{
  
const {app:{data}} = useSelector(state=>state)
return (
  <div className={style.containerProd}>
    <div className={style.containerCard}>
      { data.map(element =>
      <CardItems 
        key={element.id}
        data={element} 
        />
        )}
    </div>
  </div>
)};



export default Card;