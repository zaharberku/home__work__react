import React from "react";
import Basket from "./components"
import { GoBack } from "../../sharedComponents";


const BasketPage = () =>{
    return(
    <section>
        <GoBack/>
     <Basket/>
    </section>
)}


export default BasketPage