import React from "react";
import BasketItems from "../BasketItems";
import style from "./Basket.module.scss";
import PropTypes from "prop-types";
import { useSelector } from 'react-redux';


const Basket = () => {
    const state = useSelector(state => state)

    return (
        <div className={style.containerBasket}>
            <p>Total price:{state.basket.totalPrice}</p>
            {state.basket.basketData.map(element => {
                return <BasketItems key={element.id} data={element} />
            })}
        </div>
    )
};


export default Basket;