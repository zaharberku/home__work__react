import React from "react";
import style from "./BasketItems.module.scss";
import {Img, Button} from "../../../../sharedComponents";
import PropTypes from "prop-types";
import {dataBtnRemove, modalWindowDeclarations} from "../../../../data";
import { useDispatch } from "react-redux";
import { setIsOpenModal, setConfigModal } from "../../../../store/createActions";


const BasketItems = (props) =>{
    const {data} = props
    const { name, pathPicture:src, price, count } = data
 
    const dispatch = useDispatch()
    
    return(
    <div className={style.prodBasket}>
        <Img additionalClassNames={style.basketImg} src={src} alt={name}/>
        <p className={style.name}>{name}</p>
        <p className={style.price}>{price}</p>
        <p>Количество:{count}</p>
        <Button 
        additionalClassNames={style.btnRemove} 
        data={dataBtnRemove} 
        handelClick={()=>{
            dispatch(setConfigModal({...modalWindowDeclarations[1], prod:data}))
            dispatch(setIsOpenModal(true))
        }}/>
    </div>
)};


BasketItems.defaultProps = {
    name: 'Ноут',
    src: 'Картинка',
    price: '15000',
    count: 0,
};

BasketItems.propTypes = {
    name: PropTypes.string,
    src: PropTypes.string,
    price: PropTypes.string,
    count: PropTypes.number,
};




export default BasketItems;