import React from "react";
import Favorite from "./components";
import { GoBack } from "../../sharedComponents";


const FavoritePage = () =>{
    return(
    <section>
        <GoBack/>
        <Favorite/>
    </section>
)};


export default FavoritePage;