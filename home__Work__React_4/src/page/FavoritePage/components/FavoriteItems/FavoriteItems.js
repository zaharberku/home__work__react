import React from "react";
import {Img} from "../../../../sharedComponents";


const FavoriteItems = ({data:{name, pathPicture:src}}) =>{
    return (
        <div>
            <Img src={src} alt={name}/>
            <p>Name:{name}</p>
        </div>
    );
};

export default FavoriteItems;