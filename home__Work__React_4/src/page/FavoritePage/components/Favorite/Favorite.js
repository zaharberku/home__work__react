import React from "react";
import FavoriteItems from "../FavoriteItems";
import { useSelector } from "react-redux";


const Favorite = () =>{
    const {app:{data}} = useSelector(state=>state)
    const filteFavorite = () => data.filter(element => element.favorite)
    return(
        <div>
            { filteFavorite().map(element=> <FavoriteItems key={element.id} data={element}/>)}
        </div>
    );
};


export default Favorite;