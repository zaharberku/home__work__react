import React from "react";
import style from "./Header.module.scss";
import PropTypes from "prop-types";

const Header = ({children}) =>(
    <header className={style.header}>
        {children}
    </header>
);


Header.defaultProps = {
    children:null
};

Header.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
};

export default Header;