import React from "react";
import style from "./Modal.module.scss";
import Button from "../Button";
import { dataCloseBtn } from "../../data";
import { useSelector, useDispatch } from "react-redux";
import { setIsOpenModal, changesLocalStorDataBasket } from "../../store/createActions";


const Modal = () =>{

    const { modal:{actions, text, title, id, prod, key}} = useSelector(state => state)
    
    const closeBtn = dataCloseBtn.find( elem=> elem.id === id);
    const dispatch = useDispatch()
    
    return(
        <div onClick={()=>dispatch(setIsOpenModal(false))} className={style.windowClose}>
        <div className={style.modal}>
            <div className={style.modalHeader}>
                <h1 className={style.modalTitle}>{title}</h1>
            </div>
            <div className={style.modalBody}>
                {`${text} ${prod.name}`}
            </div>
            <div className={style.modalFooter}>
                {actions(()=>dispatch(changesLocalStorDataBasket(key, prod)))}
                {id === 1 && <Button additionalClassNames={style.closeBtn} handelClick={()=>dispatch(setIsOpenModal(false))} data={closeBtn}/>}
            </div>
        </div>
        </div>
    );
};



export default Modal;