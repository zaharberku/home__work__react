import React from "react";
import { useHistory } from "react-router-dom";
import Button from "../Button";


const dataBtnGoBack = {
    text:'go back'
};

const GoBack = () =>{
const history = useHistory();

const goBack = () =>{
    history.goBack();
};

if(history.length <= 1){return null};

return(
    <>
    {<Button handelClick={goBack} data={dataBtnGoBack} />}
    </>
);
};

export default GoBack;