import React from "react";
import List from "../List";
import {dataHeaderNav} from "../../data"
import style from "./HeaderNav.module.scss"


const HeaderNav = () =>{
    return (
        <nav>
            <ul className={style.list}>
              { dataHeaderNav.map(({id, text, path}) => <List key={id} text={text} path={path}/>)}
            </ul>
        </nav>
    )
}

export default HeaderNav