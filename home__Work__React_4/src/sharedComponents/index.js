import Button from "./Button";
import Modal from "./Modal";
import Main from "./Main";
import Header from "./Header";
import HeaderNav from "./HeaderNav";
import GoBack from "./GoBack";
import Img from "./Img";


export { GoBack, Button, Modal, Main, Header, HeaderNav, Img};