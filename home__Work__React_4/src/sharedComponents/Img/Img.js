import React from "react";
import style from "./Img.module.scss";
import PropTypes from "prop-types";
import classNames from "classnames"

const Img = ({src, alt, additionalClassNames}) =>(
  <img className={classNames(style.img, additionalClassNames)} src={src} alt={alt}/>
);



Img.defaultProps = {
  src: 'src',
  alt: 'alt',
  className: 'img'
};
 
Img.propTypes = {
  src: PropTypes.string,
  alt: PropTypes.string,
  className: PropTypes.string,
};

export default Img;