import style from './App.module.scss';
import { Modal, Product, Main, Header, Basket } from './components'
import React, { Component } from "react";
import { modalWindowDeclarations } from "./data"
import { getLocalStorage, setLocalStorage } from "./util";


class App extends Component {

  state = {
    isOpenModal: false,
    dataForModal: {},
    dataForProducts: [],
    product: {},
    basket: [],
    totalPrice: 0,
  }

  componentDidMount = async () => {
    let data = getLocalStorage("data")
    if (data.length === 0) {
      try {
        data = await fetch('./data.json').then(res => res.json())
      } catch (error) {
        console.log(error.name);
      }
      data.forEach(element => setLocalStorage("data", element))
    }
    const dataBasket = getLocalStorage('basket')
    this.setState({
      dataForProducts: data,
      basket: dataBasket,
      totalPrice: dataBasket.reduce((item, element) => item + (+element.price.replace(' ', '') * element.count), 0),
    })
  }

  closeModal = () => {
    this.setState({
      isOpenModal: false,
      dataForModal: {}
    })
  }

  openModal = (id, data) => {
    this.setState({
      isOpenModal: true,
      dataForModal: modalWindowDeclarations.find(element => element.id === +id),
      product: data,
    })
  }

  addProductBasket = (event,{ name, id, pathPicture: src, price }) => {
    event.stopPropagation()
    this.setState(current => {
      const index = current.basket.findIndex(element => element.id === id);
      const newBasket = [...current.basket];
      if (index === -1) {
        const dataProd = {
          name,
          id,
          src,
          count: 1,
          price,
        };
        newBasket.push(dataProd);
        setLocalStorage('basket', dataProd);
        return ({
          basket: newBasket,
          isOpenModal: false,
          totalPrice: current.totalPrice + +dataProd.price.replace(" ", '')
        });
      };



      newBasket[index].count++
      localStorage.setItem('basket', JSON.stringify(newBasket))
      return ({
        basket: newBasket,
        isOpenModal: false,
        totalPrice: current.totalPrice + +newBasket[index].price.replace(" ", '')
      })
    });
  }


  changesDataLocalStor = (element, bool) => {
    this.setState(({dataForProducts}) => {
      const index = dataForProducts.findIndex(elem => elem.id === element.id)
      dataForProducts[index].favorite = bool
      localStorage.setItem('data', JSON.stringify(dataForProducts))
      return ({
        dataForProducts: dataForProducts
      })
    })
  }

  render() {
    const { dataForModal, isOpenModal, dataForProducts, product, basket, totalPrice } = this.state
    return (
      <div className={style.App}>
        <Header/>
        <Main>
          <Product changesDataLocalStor={this.changesDataLocalStor} dataForProducts={dataForProducts} openModal={this.openModal} />
          <Basket  data={basket} totalPrice={totalPrice}/>
          {isOpenModal && <Modal closeModal={this.closeModal} dataForModal={dataForModal} addProductBasket={this.addProductBasket} product={product} />}
        </Main>
      </div>
    );
  }

}

export default App;
