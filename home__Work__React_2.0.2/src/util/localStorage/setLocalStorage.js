import getLocalStorage from "./getLocalStorage";


const setLocalStorage = (keys, elem) =>{
    const newData = getLocalStorage(keys);
    newData.push(elem);
    localStorage.setItem(keys,JSON.stringify(newData));
};


export default setLocalStorage;