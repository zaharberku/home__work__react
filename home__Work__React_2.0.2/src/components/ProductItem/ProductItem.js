import React, { Component } from "react";
import style from "./ProductItem.module.scss";
import Button from "../Button";
import Img from "../Img";
import PropTypes from "prop-types";
import { dataBtnCard } from "../../data";
import {ReactComponent as Favorite} from "../../assets/fav.svg"
import {ReactComponent as UnFavorite} from "../../assets/unfav.svg"



class ProductItem extends Component {

  render() {

    const {data, openModal, changesDataLocalStor } = this.props
    const { name, pathPicture, price, vendorCode, color, favorite } = data
    return (
      <div className={style.blockProduct}>
        <span className={style.favorite} onClick={()=>changesDataLocalStor(data, !favorite)}>
          {favorite ? <Favorite/> : <UnFavorite/>}
        </span>
        <Img src={pathPicture} alt={name} />
        <p><span className={style.list}>Название:</span>{name}</p>
        <p><span className={style.list}>Цена:</span>{price}</p>
        <p><span className={style.list}>Код товара:</span>{vendorCode}</p>
        <p><span className={style.list}>Цвет:</span>{color}</p>
        <Button additionalClassNames={style.btn} data={dataBtnCard} handelClick={() => openModal(dataBtnCard.id, data)} />
      </div>
    );
  };
};



ProductItem.defaultProps = {
  data: {
    name: 'name',
    pathPicture: 'pathPicture',
    price: '1000',
    vendorCode: '12313123wqeq',
    color: 'white',
    id: 0,
  },
  data: {},
  openModal: () => { },
  dataProd: [],
  changesProduct: () => { },
  favorite: [],
};

ProductItem.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    pathPicture: PropTypes.string,
    price: PropTypes.string,
    vendorCode: PropTypes.string,
    color: PropTypes.string,
    id: PropTypes.number,
  }),
  data: PropTypes.object,
  openModal: PropTypes.func,
  dataProd: PropTypes.arrayOf(PropTypes.object),
  changesProduct: PropTypes.func,
  favorite: PropTypes.arrayOf(PropTypes.object),
};

export default ProductItem;