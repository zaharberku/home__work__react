import React, { Component } from "react";
import style from "./ProductBasket.module.scss";
import Img from "../Img";
import PropTypes from "prop-types";

class ProductBasket extends Component{
    render(){
        const { name, src, price, count } = this.props;
        return(
            <div className={style.prodBasket}>
                <Img additionalClassNames={style.img} src={src} alt={name}/>
                <p className={style.name}>{name}</p>
                <p className={style.price}>{price}</p>
                <p>Количество:{count}</p>
            </div>
        );
    };
};


ProductBasket.defaultProps = {
    name: 'Ноут',
    src: 'Картинка',
    price: '15000',
    count: 0,
};

ProductBasket.propTypes = {
    name: PropTypes.string,
    src: PropTypes.string,
    price: PropTypes.string,
    count: PropTypes.number,
};




export default ProductBasket;