import React,{Component} from "react";
import PropTypes from "prop-types";
import style from "./Modal.module.scss";
import Button from "../Button";
import { dataCloseBtn } from "../../data";




class Modal extends Component{
    render(){
        const { dataForModal: {actions, title, id }, closeModal, product, addProductBasket } = this.props;
        const closeBtn = dataCloseBtn.find( elem=> elem.id === id);
        return(
            <div onClick={closeModal} className={style.windowClose}>
            <div className={style.modal} >
                <div className={style.modalHeader}>
                    <h1 className={style.modalTitle}>{title}</h1>
                </div>
                <div className={style.modalBody}>
                    {product.name}
                </div>
                <div className={style.modalFooter}>
                    {actions((event)=>addProductBasket(event,product))}
                    <Button additionalClassNames={style.btnCancel} handelClick={closeModal} data={closeBtn}/>
                </div>
            </div>
            </div>
        );
    };
};

Modal.defaultProps = {
    dataForModal: {
        actions: <button></button>,
        text: 'text',
        title: 'title',
        id:0,
    },
    closeModal:()=>{},
    product:{},
    addProductBasket:()=>{},
};

Modal.protoType = {
    dataForModal: PropTypes.shape({
        actions: PropTypes.element,
        text: PropTypes.string,
        title: PropTypes.string,
        id: PropTypes.number,
    }),
    closeModal: PropTypes.func,
    product: PropTypes.object,
    addProductBasket: PropTypes.func,
};


export default Modal;