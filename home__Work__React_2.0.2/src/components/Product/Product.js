import React,{Component} from "react";
import ProductItem from "../ProductItem";
import style from "./Product.module.scss";
import PropTypes from "prop-types";
import Basket from "../Basket";


class Product extends Component{
    
   
    render(){
        const {dataForProducts, openModal, changesDataLocalStor} = this.props;
        return(
            <div className={style.containerCard}>
              { dataForProducts.map(element =>
              <ProductItem 
                key={element.id}
                data={element} 
                openModal={openModal}
                changesDataLocalStor={changesDataLocalStor}
                dataProd={dataForProducts}/>
                )}
            </div>
        );
    };
};

Product.defaultProps = {
  dataForProducts: [],
  openModal: ()=>{},
  basket:[],
  totalPrice:0,
  favorite:[],
};

Product.propTypes = {
  dataForProducts: PropTypes.arrayOf(PropTypes.object),
  openModal: PropTypes.func,
  basket: PropTypes.arrayOf(PropTypes.object),
  totalPrice:PropTypes.number,
  favorite:PropTypes.arrayOf(PropTypes.object),
};


export default Product;