import React,{Component} from "react";
import ProductBasket from "../ProductBasket";
import style from "./Basket.module.scss";
import PropTypes from "prop-types";

class Basket extends Component{

    render(){
        const { data, totalPrice } = this.props;
        return(
            <div className={style.containerBasket}>
                <p>Total price:{totalPrice}</p>
                { data.map(({ name, src, price, id, count }) => <ProductBasket key={id} name={name} src={src} price={price} count={count} />) }
            </div>
        );
    };
};

Basket.defaultProps = {
    data: [],
    totalPrice:0,
};


Basket.propTypes = {
    data: PropTypes.arrayOf(PropTypes.object),
    totalPrice: PropTypes.number,
};



export default Basket;