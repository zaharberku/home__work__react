import React,{Component} from "react";
import style from "./Img.module.scss";
import PropTypes from "prop-types";


class Img extends Component{
    render(){
        const {src, alt, additionalClassNames} = this.props ;
        return(
          <img className={`${style.img} ${additionalClassNames}`} src={src} alt={alt}/>
        );
    };
};

Img.defaultProps = {
  src: 'src',
  alt: 'alt',
  className: 'img'
};
 
Img.propTypes = {
  src: PropTypes.string,
  alt: PropTypes.string,
  className: PropTypes.string,
};

export default Img;