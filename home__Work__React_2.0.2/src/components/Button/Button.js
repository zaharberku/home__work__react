import React, { Component } from "react";
import PropTypes from "prop-types";
import style from "./Button.module.scss";



class Button extends Component {
    render() {
        const { data: { text, background }, handelClick, additionalClassNames } = this.props;

        return (
            <button className={`${style.btn} ${additionalClassNames}` } onClick={handelClick} background={background}>{text}</button>
        );
    };
};

Button.defaultProps = {
    data: {
        background: '#fff',
    },
    handelClick: () => { },
    className: 'btn',
};

Button.propTypes = {
    data: PropTypes.shape({
        text: PropTypes.string.isRequired,
        background: PropTypes.string,
    }),
    handelClick: PropTypes.func,
    className: PropTypes.string,
};

export default Button;

