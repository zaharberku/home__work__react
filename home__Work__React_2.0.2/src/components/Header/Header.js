import React,{ Component} from "react";
import style from "./Header.module.scss";
import PropTypes from "prop-types";


class Header extends Component{
    render(){
        const {children} = this.props
        return(
            <header className={style.header}>
                {children}
            </header>
        );
    };
};

Header.defaultProps = {
    children:null
};

Header.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
};

export default Header;