import style from "../components/Modal/Modal.module.scss"

const modalWindowDeclarations = [
  {
    id: 1,
    text: 'Modal First',
    title: 'Basket',
    actions:(func) => <button onClick={func} className={style.modalFooterBtn} >Save</button>,
  },
  {
    id: 2,
    text: 'Second First',
    title: 'Title2',
    actions: <button className={style.modalFooterBtn}>More</button>,
  },
];


const dataCloseBtn = [
  {
    text: "Cancel",
    background: "black",
    id: 1,
  },
  {
    text: "x",
    background: "black",
    id: 2,
  }

];

const dataBtnCard = {
  id: 1,
  background: "#e8b692",
  text: "Add to cart",
};

export { modalWindowDeclarations, dataCloseBtn, dataBtnCard };