import getElementLocalStorage from "./getElementLocalStorage"


function addElementLocalStorage(keys,elem){
    const dataLocalStorge = getElementLocalStorage(keys)
    dataLocalStorge.push(elem)
    localStorage.setItem(keys, JSON.stringify(dataLocalStorge))
  }


export default addElementLocalStorage