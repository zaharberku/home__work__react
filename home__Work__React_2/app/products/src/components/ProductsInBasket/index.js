import React,{Component} from "react";




class ProductsInBasket extends Component{
    render(){
        const {img, name, deleteProductWithBasket, favorite = true} = this.props
        return( 
            <li className="basket__item" onClick={deleteProductWithBasket}>
                <img className="basket__img" src={img} alt={name}/>
                <p  className="basket__text">{name}</p>
                {favorite && <button>x</button>}
            </li>
        )
    }
}

export default ProductsInBasket