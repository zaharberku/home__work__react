import React,{Component} from "react";
import ProductsInBasket from "../ProductsInBasket";
import { getElementLocalStorage } from "../localStorage";


class Basket extends Component{
    render(){
        const {data, deletElementWithLocalStorage} = this.props
        
        return(
            <div className="main__basket basket">
                <h1 className="basket__title">Basket</h1>
                <div>
                    <ul className="basket__list">
                        {data.map(({name,pathPicture,id})=> <ProductsInBasket key={id} name={name} img={pathPicture} deleteProductWithBasket={(event)=>deletElementWithLocalStorage(event,id,'basket')}/>)}
                    </ul>
                </div>
            </div>
        )
    }
}

export default Basket