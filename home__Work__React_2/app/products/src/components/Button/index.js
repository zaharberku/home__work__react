import React,{Component} from "react";


class Button extends Component{
    render(){
        const{dataForBtn:{background,textBtnSecond}}= this.props;

        return(
                <header className="App__header header">
                <button className="header__btn" data-id="2" background={background}>{textBtnSecond}</button>
                </header>
        )
    }
}

export default Button