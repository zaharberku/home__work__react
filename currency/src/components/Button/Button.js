import React from "react";
import classNames from "classnames"


const Button = (props) => {
    const {text, handelClick, additionalClassNames} = props;
    return(
        <button onClick={handelClick} className={classNames(additionalClassNames)}>{text}</button>
    );
};


export default Button