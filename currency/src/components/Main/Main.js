import React, {useEffect, useRef} from "react";
import styles from "./Main.module.scss";
import Input from "../Input";
import Select from 'react-select';
import { options, colourStyles } from "../../data";
import { getBaseCcy, getCcy, getValueInput, changeInputValue } from "../../store/createActions/createActions";
import { useDispatch, useSelector } from "react-redux";
import bigBoss from "../../assets/images/big-boss.png";

const Main = () => {
    const dispatch = useDispatch();
    const {valueInputFirst, valueInputSecond} = useSelector(state=>state);
    return (
        <main className={styles.main}>
            <div className={styles.main__container}>
                <div>
                    <h1 className={styles.main__title_text}>Instant currency exchange</h1>
                    <div className={styles.main__currency}>
                        <div className={styles.main__input_and_select}>
                            <Select 
                            styles={colourStyles} 
                            options={options} 
                            onChange={({value})=>{
                                dispatch(getCcy(value)) 
                                dispatch(changeInputValue({
                                    nameFirst:'valueInputFirst',
                                    nameSecond:'valueInputSecond'
                                }))
                                }} />
                            <Input 
                            value={valueInputFirst}
                            type={'number'}
                            handelInput={({target})=>{
                                dispatch(getValueInput({name:'valueInputFirst',value:target.value}))
                                dispatch(changeInputValue(
                                    {
                                        nameFirst:'valueInputFirst',
                                        nameSecond:'valueInputSecond'
                                    }
                                ))
                            }} 
                            additionalClassNames={styles.main__input} 
                            placeholder={'enter price'} />
                        </div>
                        <div className={styles.main__input_and_select}>
                            <Select 
                            styles={colourStyles} 
                            options={options} 
                            onChange={({value})=>{
                                dispatch(getBaseCcy(value))
                                dispatch(changeInputValue({
                                    nameFirst:'valueInputSecond',
                                    nameSecond:'valueInputFirst'
                                }))
                                }} />
                            <Input 
                            value={valueInputSecond}
                            type={'number'}
                            handelInput={({target})=>{
                                dispatch(getValueInput({name:'valueInputSecond',value:target.value}))
                                dispatch(changeInputValue(
                                    {
                                        nameFirst:'valueInputSecond',
                                        nameSecond:'valueInputFirst'
                                    }
                                ))
                            }} 
                            additionalClassNames={styles.main__input} 
                            placeholder={'enter price'} />
                        </div>
                    </div>
                </div>
                <img src={bigBoss} alt="big-boss" />
            </div>
        </main>
    );
};
export default Main