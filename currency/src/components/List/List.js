import React from 'react';


const List = (props) => {
    const {text} = props
    return (
        <li>{text}</li>
    )
}

export default List