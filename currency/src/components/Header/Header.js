import React from 'react';
import styles from './Header.module.scss';
import { GlobalSvg } from '../../assets';
import { useSelector } from 'react-redux';
import List from '../List';
import Button from '../Button';
import { findCcy, addCcy } from '../../util';
import classNames from 'classnames';

const Header = () => {
    const {header, currency} = useSelector(state=>state);
    const {nav = [], nameSvg = '', btn = '', USD, EUR } = header;
    const dataUSD = findCcy(currency, USD);
    const dataEUR = findCcy(currency, EUR);

    return (
        <header className={styles.header}>
            <div className={classNames(styles.header__nav, styles.nav)}>
                <div className={styles.nav__title}>
                    <span className={styles.nav__title_svg}>
                    <GlobalSvg type={nameSvg} />
                    </span>
                    <h1 className={styles.nav__title_text}>B-bank</h1>
                </div>
                <ul className={styles.nav__list}>
                    {nav.map(({id, text}) =><List key={id} text={text}/>)}
                </ul>
                <div className={styles.nav__cuurrency}>
                {dataUSD && <p><GlobalSvg type={USD} />{`${dataUSD.buy}/${dataUSD.sale}`}</p>}
                {dataEUR && <p><GlobalSvg type={EUR} />{`${dataEUR.buy}/${dataEUR.sale}`}</p>}
                </div>
                <div className={styles.nav__container_btn}>
                <Button additionalClassNames={styles.nav__btn} text={btn.text}/>
                </div>
            </div>
        </header>
    );
};

export default Header