import React from 'react';
import styles from './Input.module.scss'
import classNames from 'classnames';


const Input = (props) => {
    const { value, handelInput, additionalClassNames, type, placeholder } = props
    return (
        <input value={value} placeholder={placeholder} type={type} onInput={handelInput} className={classNames(additionalClassNames)}/>
    )
}

export default Input