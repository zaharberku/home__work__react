import { GET_DATA_CURRENCY, GET_DATA_HEADER, GET_CCY, GET_BASE_CCY, CHANGE_VALUE_INPUT, GET_INPUT_VALUE } from '../actions/actions';
import { findCcy } from "../../util";



export const getDataCurrency = () => async (dispatch) => {
    const data = await fetch('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5').then(res=>res.json());
    const usd = findCcy(data, 'USD');
    const eur = findCcy(data, 'EUR');
    const newUsd = {
        ccy:usd.ccy,
        base_ccy:eur.ccy,
        buy:`${(usd.buy / eur.buy).toFixed(2)}`,
        sale:`${(eur.buy / usd.buy).toFixed(2)}`,
    };
    const newEur = {
        ccy:eur.ccy,
        base_ccy:usd.ccy,
        buy:`${(eur.buy / usd.buy).toFixed(2)}`,
        sale:`${(usd.buy / eur.buy).toFixed(2)}`,
    };
    data.push(newUsd)
    data.push(newEur)

    const newData = [...data].map(element =>{
        element.buy = element.buy.slice(0,5)
        element.sale = element.sale.slice(0,5)
        return element
    });
    

    dispatch({type:GET_DATA_CURRENCY, payload:newData});
};

export const getDataHeader = (data) => ({type:GET_DATA_HEADER, payload: data});

export const getCcy = (ccy) => ({type:GET_CCY, payload: ccy});

export const getBaseCcy = (baseCcy) => ({type:GET_BASE_CCY, payload: baseCcy});

export const changeInputValue = (payload) => ({type:CHANGE_VALUE_INPUT, payload});

export const getValueInput = (value) => ({type:GET_INPUT_VALUE, payload:value});