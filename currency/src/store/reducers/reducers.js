import { GET_DATA_CURRENCY, GET_DATA_HEADER, GET_CCY, GET_BASE_CCY, GET_INPUT_VALUE, CHANGE_VALUE_INPUT } from "../actions/actions";
const initialState = {
    currency:[],
    header:{},
    ccy:'',
    base_ccy:'',
    valueInputFirst:0,
    valueInputSecond:0,
};


export const reducers = (state = initialState, actions) => {
    const {type, payload} = actions;
    const newCurrency = [...state.currency]
    switch(type){
        case GET_DATA_CURRENCY:
            return {...state, currency: payload}
        case GET_DATA_HEADER:
            return {...state, header: payload}
        case GET_CCY:
            return {...state, ccy:payload}
        case GET_BASE_CCY:
            return {...state, base_ccy:payload}
        case CHANGE_VALUE_INPUT:
            const elemFirst = newCurrency.find(element => element.ccy === state.ccy && element.base_ccy === state.base_ccy)
            if(elemFirst){
                return {...state, [payload.nameSecond]: (state[payload.nameFirst] * elemFirst.buy).toFixed(3)}
            };
            const elemSecond = newCurrency.find(element => element.base_ccy === state.ccy && element.ccy === state.base_ccy)
            if(elemSecond){
                return {...state, [payload.nameSecond]:  (state[payload.nameFirst] / elemSecond.buy).toFixed(3)}
            };
            return state
        case GET_INPUT_VALUE:
            return {...state, [payload.name]:payload.value}
        default:
            return state
    };
};

 