import React, {useEffect} from "react";
import { Header, Main } from "./components";
import { dataHeader } from "./data";
import { useDispatch } from "react-redux";
import { getDataCurrency, getDataHeader, addCcy } from "./store/createActions/createActions";

const App = () => {
  const dispatch = useDispatch()

  useEffect(()=>{
    dispatch(getDataHeader(dataHeader));
    try{
      dispatch(getDataCurrency())
    }catch(err){
      console.log(err);
    };
  },[])


  return (
    <div className="container">
      <Header/>
      <Main/>
    </div>
  );
};

export default App;
